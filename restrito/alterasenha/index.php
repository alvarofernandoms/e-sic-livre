<?php
/**********************************************************************************
 Sistema e-SIC Livre: sistema de acesso a informa��o baseado na lei de acesso.
 
 Copyright (C) 2014 Prefeitura Municipal do Natal
 
 Este programa � software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos da Licen�a GPL2.
***********************************************************************************/

 include("manutencao.php");
 
 include("../inc/topo.php");
?>
<div role="main" class="col-md-8">
<h1>Redefini&ccedil;&atilde;o de Senha</h1>
<br><br>
<form action="index.php" id="formulario" method="post">
<table id="tabelaSolucaoCidada" align="center" cellpadding="0" cellspacing="1">
     <div class="form-group"> 
	<tr>
		<td> <label for="inputName"><b>Senha atual:</b></label></td>
		<td><input type="password" name="senhaatual" size="50" maxlength="50" class="form-control" id="senhaatual" /> </td>
	</tr>
        </div>
         <div class="form-group"> 
	<tr>
		         <td> <label for="inputName"><b>Nova senha:</b></label></td>           
		<td><input type="password" name="novasenha" size="50" maxlength="50" name="Novasenha" id="novasenha" /> </td>
	</tr>
        </div>  
        <div class="form-group">
	<tr>
		<td> <label for="inputName"><b>Confirme a nova senha:</b></label></td>     
		<td><input type="password" name="confirmasenha" size="50" maxlength="50" name="confirmenovasenha" id="confirmenovanovasenha" /> </td>
	</tr>	
	</div>
	
	<tr><td colspan="2"><td></tr>
	<tr>
		<td colspan="2" align="center" style="border-top:1px solid #000000">
			<br><input type="submit" value="Alterar" class="botaoformulario" name="acao" />
		</td>
	</tr>
</table>
</form>
</div>
<?php 
getErro($erro);
include("../inc/rodape.php");?>
