<?php
/**********************************************************************************
 Sistema e-SIC Livre: sistema de acesso a informa��o baseado na lei de acesso.
 
 Copyright (C) 2014 Prefeitura Municipal do Natal
 
 Este programa � software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos da Licen�a GPL2.
***********************************************************************************/

include("../inc/topo.php"); 	
?>

	<span><p><h1>Manuais do sistema</h1></p></span><br><br>
	
	- <a href="Manual_Administrador.pdf" target="_blank">Manual do Administrador do Sistema</a><br><br>
	- <a href="Manual_Usuario.pdf" target="_blank">Manual do Usu&aacute;rio do Sistema</a>
	
<?php include("../inc/rodape.php"); ?>
